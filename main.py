

from fastapi import FastAPI, UploadFile, HTTPException

from fastapi.middleware.cors import CORSMiddleware
from user.auth import auth_backend
from chat.router import router
from user import userRouter
from user.schemas import UserRead, UserCreate, UserUpdate,UserProfile

app = FastAPI(
    title="Messenger"
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(
    userRouter.fastapi_users.get_auth_router(auth_backend),
    prefix="/api/v1/auth/jwt",
    tags=["auth"],

)

app.include_router(
    userRouter.fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/api/v1/auth",
    tags=["auth"],
)

app.include_router(router=userRouter.router, prefix="/api/v1", tags=["user"])
app.include_router(router=router, prefix="/api/v1", tags=["chat"])



