from typing import Dict

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from chat.crud import save_message, group_create, get_group, upload_file
from fastapi import WebSocket, WebSocketDisconnect, Depends, HTTPException, UploadFile
from fastapi import APIRouter, status

from user.models import User
from user.userRouter import current_user
from .schemas import MessageRead, GroupCreate, GroupRole, GroupRoleCreate, GroupMembers
from chat.models import Group, UserGroupRelationship
from core.database import get_async_session, async_session_maker
from contextlib import asynccontextmanager





@asynccontextmanager
async def websocket_session():
    async with async_session_maker() as session:
        try:
            yield session
        finally:
            session.close()


router = APIRouter()
origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
]


class ConnectionManager:
    def __init__(self):
        self.active_connections: Dict[int, WebSocket] = {}

    async def connect(self, websocket: WebSocket, client_id: int):
        await websocket.accept()
        self.active_connections[client_id] = websocket

    def disconnect(self, client_id: int):
        del self.active_connections[client_id]

    async def send_message(self, message: MessageRead, client_id: int):
        websocket = self.active_connections.get(client_id)

        if websocket:
            await websocket.send_json(message.model_dump())

    async def send_group_message(self, message: MessageRead, client_id: int):
        async with websocket_session() as session:
            db = await session.execute(select(UserGroupRelationship).where(UserGroupRelationship.group == client_id))
            await session.close()
            for user in db.scalars():
                websocket = self.active_connections.get(user.user)
                if websocket:
                    await websocket.send_json(message.model_dump())


manager = ConnectionManager()


@router.websocket("/ws/{client_id}")
async def websocket_endpoint(websocket: WebSocket, client_id: int):
    await manager.connect(websocket, client_id)
    try:
        while True:
            json_message = await websocket.receive_json()
            print(json_message)
            json_message["sender_id"] = client_id
            result = await save_message(json_message)
            if json_message["type"] == "direct":
                await manager.send_message(message=result, client_id=json_message["recipient_id"])
            else:
                await manager.send_group_message(message=result, client_id=json_message["recipient_id"])
    except WebSocketDisconnect:
        manager.disconnect(client_id)


@router.post("/group/create/{user_id}", response_model=GroupMembers)
async def create_group(group: GroupCreate, user_id: int, user: User = Depends(current_user)):
    db_group = Group(title=group.title, avatar=group.avatar, description=group.description)
    db = async_session_maker()
    db.add(db_group)
    await db.commit()
    db_group_role = await group_create(group=GroupRoleCreate(group=db_group.id, user=user.id, role="admin"))
    db_role = db_group_role.__dict__
    del db_role["_sa_instance_state"]
    await db.refresh(db_group)
    await db.close()
    return GroupMembers(id=db_group.id,
                        title=db_group.title,
                        description=db_group.description,
                        avatar=db_group.avatar,
                        members=[db_role])


@router.post("/group/add_member/")
async def add_group_member(group_role: GroupRoleCreate):
    db = async_session_maker()
    try:
        check = await db.execute(select(UserGroupRelationship).where(UserGroupRelationship.user == group_role.user))
        if check is None:
            db_group_role = UserGroupRelationship(user=group_role.user, group=group_role.group,
                                                  role=group_role.role.value)
            db.add(db_group_role)
            await db.commit()
            await db.refresh(db_group_role)
            await db.close()
            return status.HTTP_201_CREATED
        else:
            return status.HTTP_208_ALREADY_REPORTED
    except:
        return status.HTTP_500_INTERNAL_SERVER_ERROR


@router.get("/get_group_info/{group_id}", response_model=GroupMembers)
async def get_group_info(group_id: int, session: AsyncSession = Depends(get_async_session)):
    group = await get_group(group_id, session)
    if group is None:
        raise HTTPException(status_code=404, detail="Group not found")
    members = await session.execute(select(UserGroupRelationship).where(UserGroupRelationship.group == group_id))
    members = members.scalars()
    group_member = GroupMembers(
        id=group_id,
        title=group.title,
        description=group.description,
        avatar=group.avatar,
        members=[member.__dict__ for member in members])
    await session.close()
    return group_member


@router.post("/uploadfile/")
async def create_upload_file(file: UploadFile):
    file = await upload_file(file)
    return {"filename": f"{file}"}
