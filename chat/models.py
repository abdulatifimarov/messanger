from datetime import datetime

from sqlalchemy import Column, Integer, String, Text, TIMESTAMP, Boolean, ForeignKey

from core.base import Base
from user.models import User, Avatar


class Group(Base):
    __tablename__ = "group"
    id = Column(Integer, primary_key=True, index=True, unique=True)
    createdAt = Column(TIMESTAMP, default=datetime.utcnow)
    title = Column(String(50), nullable=False)
    description = Column(Text)
    avatar = Column(Integer, ForeignKey("avatar.id", onupdate="CASCADE", ondelete="SET NULL"))


class UserGroupRelationship(Base):
    __tablename__ = "user_group_relationship"
    id = Column(Integer, primary_key=True, index=True, unique=True)
    user = Column(Integer, ForeignKey("user.id", ondelete="CASCADE"))
    group = Column(Integer, ForeignKey("group.id", ondelete="CASCADE"))
    role = Column(String(6), nullable=False, default='member')


class Message(Base):
    __tablename__ = "message"
    id = Column(Integer, primary_key=True, index=True, unique=True)
    text = Column(Text)
    createdAt = Column(TIMESTAMP, default=datetime.utcnow)
    updatedAt = Column(TIMESTAMP, default=datetime.utcnow)
    recipient_id = Column(Integer)
    sender_id = Column(Integer, ForeignKey("user.id", ondelete="CASCADE"))
    type = Column(String, nullable=False)
    # is_read = Column(Boolean)


# class Is_Read(Base):
#     __tablename__ = "is_read_status"
#     id = Column(Integer, primary_key=True, index=True, unique=True)
#     recipient = Column(Integer, ForeignKey("user.id", ondelete="NO ACTION"))
#     message_id = Column(Integer, ForeignKey("message.id", ondelete="CASCADE"))
#     is_read = Column(Boolean, default=False)
