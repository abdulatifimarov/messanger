from datetime import date
import os
import shutil

from fastapi import UploadFile, HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from .schemas import *
from core.database import async_session_maker
from .models import Message, Group, UserGroupRelationship


async def save_message(message: MessageCreate):
    print(message)
    db_message = Message(**message)
    print(message)
    db = async_session_maker()
    db.add(db_message)
    await db.commit()
    print(db_message.sender_id)
    await db.refresh(db_message)
    await db.close()
    return MessageRead(
        id=db_message.id,
        sender_id=db_message.sender_id,
        recipient_id=db_message.recipient_id,
        createdAt=db_message.createdAt.strftime('%Y-%m-%dT%H:%M:%S.%f'),
        updatedAt=db_message.updatedAt.strftime('%Y-%m-%dT%H:%M:%S.%f'),
        type=db_message.type,
        text=db_message.text
    )


async def group_create(group: GroupRoleCreate):
    db = async_session_maker()
    db_group_role = UserGroupRelationship(user=group.user, group=group.group, role=group.role.value)
    db.add(db_group_role)
    await db.commit()
    await db.refresh(db_group_role)
    await db.close()
    return db_group_role


async def get_group(group_id: int, session: AsyncSession):
    group = await session.get(Group, group_id)
    return group


async def upload_file(file: UploadFile):
    content_type = file.content_type
    if content_type not in ["image/jpeg", "image/png", "image/gif"]:
        raise HTTPException(status_code=400, detail="Invalid file type")
    upload_dir = os.path.join(os.getcwd(), f"media\\avatar\\{date.today()}")
    # Create the upload directory if it doesn't exist
    if not os.path.exists(upload_dir):
        os.makedirs(upload_dir)

    # get the destination path
    dest = os.path.join(upload_dir, file.filename)

    # copy the file contents
    with open(dest, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)
        loc = buffer.name

    return loc
