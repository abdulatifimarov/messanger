from datetime import datetime
from typing import Optional, List

from user.schemas import UserRead, UserProfile
from pydantic import BaseModel, Field
from enum import Enum


class SentTo(Enum):
    direct = "direct"
    group = "group"


class MessageCreate(BaseModel):
    text: str
    sender_id: int
    recipient_id: int
    type: SentTo


class MessageRead(BaseModel):
    id: int
    text: str
    sender_id: int
    recipient_id: int
    createdAt: str
    updatedAt: str
    type: SentTo

    class Config:
        use_enum_values = True


class GroupCreate(BaseModel):
    title: str
    description: str
    avatar: Optional[int]

    class Config:
        use_enum_values = True


#
# class GroupRead(BaseModel):
#     title: str
#     description: str
#     avatar: Optional[int]


class GroupRoles(Enum):
    admin = "admin"
    member = "member"


class GroupRole(BaseModel):
    user: int
    role: GroupRoles


class GroupRoleCreate(BaseModel):
    user: int
    group: int
    role: GroupRoles = Field(default="member")

    # class Config:
    #     orm_mode = True


class GroupMembers(BaseModel):
    id: int
    title: str
    description: str
    avatar: Optional[int]
    members: list[GroupRole]

    # class Config:
    #     orm_mode = True
