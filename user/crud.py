from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from .models import User


async def get_user(user_id: int, session: AsyncSession):
    user = await session.execute(select(User).where(User.id == user_id))
    return user.scalar()