from typing import Optional

from fastapi_users import schemas
from pydantic import BaseModel


class UserRead(schemas.BaseUser[int]):
    first_name: str
    last_name: str
    avatar: Optional[int]
    username: str
    phone: str
    description: str
    is_online: bool

    # this fields are already in BaseUser
    # id: models.ID
    # email: EmailStr
    # is_active: bool = True
    # is_superuser: bool = False
    # is_verified: bool = False


class UserCreate(schemas.BaseUserCreate):
    first_name: str
    last_name: str
    avatar: Optional[int] = None
    username: str
    phone: str
    description: str
    is_online: bool

    # this fields are already in BaseUserCreate
    # email: EmailStr
    # password: str
    # is_active: Optional[bool] = True
    # is_superuser: Optional[bool] = False
    # is_verified: Optional[bool] = False


class UserProfile(BaseModel):
    id: int
    first_name: str
    last_name: str
    avatar: Optional[int] = None
    username: str
    phone: str
    description: str
    is_online: bool
    email: str


class UserUpdate(schemas.BaseUserUpdate):
    first_name: Optional[str]
    last_name: Optional[str]
    is_online: Optional[bool]
