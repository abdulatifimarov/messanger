from datetime import datetime

from fastapi import Depends
from fastapi_users_db_sqlalchemy import SQLAlchemyUserDatabase, SQLAlchemyBaseUserTable
from sqlalchemy import Column, Integer, String, Text, TIMESTAMP, Boolean, ForeignKey
from sqlalchemy.ext.asyncio import AsyncSession

from core.base import Base
from core.database import get_async_session


class User(SQLAlchemyBaseUserTable[int], Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True, index=True, unique=True)
    email = Column(String(length=320), unique=True, index=True, nullable=False)
    first_name = Column(String(length=120))
    last_name = Column(String(length=120))
    avatar = Column(Integer, ForeignKey("avatar.id", onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    username = Column(String(length=120), unique=True, index=True, )
    phone = Column(String(length=13), unique=True, index=True, )
    description = Column(Text)
    is_online = Column(Boolean, default=False, nullable=False)
    hashed_password = Column(String(length=1024), nullable=False)
    createdAt = Column(TIMESTAMP, default=datetime.utcnow)
    is_superuser = Column(Boolean, default=False, nullable=False)
    is_active = Column(Boolean, default=True, nullable=False)
    is_verified = Column(Boolean, default=False, nullable=False)

    def full_name(self):
        return self.first_name + " " + self.last_name


class Avatar(Base):
    __tablename__ = "avatar"
    id = Column(Integer, primary_key=True, index=True, unique=True)
    location = Column(String(200))
    filetype = Column(String(50))


async def get_user_db(session: AsyncSession = Depends(get_async_session)):
    yield SQLAlchemyUserDatabase(session, User)
