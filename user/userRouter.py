from fastapi import APIRouter, Depends
from fastapi_users import FastAPIUsers
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from core.database import get_async_session
from user.schemas import UserProfile
from user.auth import auth_backend
from user.manager import get_user_manager
from user.models import User

router = APIRouter()
fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)

current_user = fastapi_users.current_user()


@router.get("/user-profile", response_model=UserProfile)
async def get_user_profile(user: User = Depends(current_user)):
    return user


@router.get("/users", response_model=list[UserProfile])
async def get_user_list(session: AsyncSession = Depends(get_async_session)):
    users = await session.execute(select(User))
    return users.scalars()
