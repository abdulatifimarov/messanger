# FastAPI Messenger Project



## Project Structure

The project is structured as follows:

- `alembic/`: Includes Alembic configuration and migrations for database management.
- `requirements.txt`: Lists the project dependencies.

## Prerequisites

Ensure you have the following installed on your system:

- [Python](https://www.python.org/) (>= 3.7)

## Installation

1. Clone the repository:

    ```bash
    git clone https://github.com/your-username/fastapi-messenger.git
    ```

2. Navigate to the project directory:

    ```bash
    cd chat
    ```

3. Install dependencies using Poetry:

    ```bash
    pip install -r requirements.txt
    ```

4. Run Alembic migrations to set up the database:
   - firstly in `alembic.ini` file and set your database url to `sqlalchemy.url`
   - secondly in `core/database.py` file and set your database url to `SQLALCHEMY_DATABASE_URL`
   
    ```bash
    alembic revision -m "initial migration"
    alembic upgrade head
    ```

## Running the Application

Use the following command to run the FastAPI development server:

```bash
uvicorn app.main:app --reload

